package br.ucsal.bd2.ExercicioJPA2.domain;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.ucsal.bd2.ExercicioJPA2.converters.SituacaoVendedorConverter;

@Entity
@Table(name = "tab_vendedor")
public class Vendedor extends Funcionario{

	/*Double percentualComissao - numeric(10,2) - not null
SituacaoVendedorEnum situacao - char(3) - not null
List<PessoaJuridica> clientes */
	
	@Column(columnDefinition = "numeric(10, 2)", nullable = false)
	private Double percentualComissao;
	
	@Convert(converter = SituacaoVendedorConverter.class)
	private SituacaoVendedorEnum situacao;
	
	@OneToMany(targetEntity = PessoaJuridica.class)
	@CollectionTable(name = "tab_vend_cliente", foreignKey = @ForeignKey(name="fk_vend_nomePJ"),
	joinColumns = @JoinColumn(name = "nome_pessoa_juridica"),
			uniqueConstraints = {
					@UniqueConstraint(name = "uk_vend_cliente", columnNames = {"clientes_cnpj"})})
	private List<PessoaJuridica> clientes;

	public Vendedor() {
		super();
	}

	public Vendedor(Double percentualComissao, SituacaoVendedorEnum situacao, List<PessoaJuridica> clientes) {
		super();
		this.percentualComissao = percentualComissao;
		this.situacao = situacao;
		this.clientes = clientes;
	}

	public Double getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(Double percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public SituacaoVendedorEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedorEnum situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientes == null) ? 0 : clientes.hashCode());
		result = prime * result + ((percentualComissao == null) ? 0 : percentualComissao.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vendedor other = (Vendedor) obj;
		if (clientes == null) {
			if (other.clientes != null)
				return false;
		} else if (!clientes.equals(other.clientes))
			return false;
		if (percentualComissao == null) {
			if (other.percentualComissao != null)
				return false;
		} else if (!percentualComissao.equals(other.percentualComissao))
			return false;
		if (situacao != other.situacao)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vendedor [percentualComissao=" + percentualComissao + ", situacao=" + situacao + ", clientes="
				+ clientes + "]";
	}
	
}
