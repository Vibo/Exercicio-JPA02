package br.ucsal.bd2.ExercicioJPA2.converters;

import javax.persistence.AttributeConverter;

import br.ucsal.bd2.ExercicioJPA2.domain.SituacaoVendedorEnum;

public class SituacaoVendedorConverter implements AttributeConverter<SituacaoVendedorEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoVendedorEnum attribute) {
		if (attribute != null) {
			return attribute.getCodigo();
		}
		return null;
	}

	@Override
	public SituacaoVendedorEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return SituacaoVendedorEnum.valueOfCodigo(dbData);
	}

}
