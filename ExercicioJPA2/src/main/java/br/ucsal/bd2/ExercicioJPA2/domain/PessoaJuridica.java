package br.ucsal.bd2.ExercicioJPA2.domain;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	/*String cnpj - char(11) - not null
	String nome - varchar(40) - not null
	List<RamoAtividade> ramosAtividade
	Double faturamento - num�rica(10,2) - not null
	List<Vendedor> vendedores */
	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cnpj;
	
	@Column(length = 40, nullable = false)
	private String nome;
	
	@OneToMany(targetEntity = RamoAtividade.class)
	@CollectionTable(name = "tab_pj_ramos", foreignKey = @ForeignKey(name="fk_ra_cnpj"), 
	joinColumns = @JoinColumn(name = "cnpj_pessoa_juridica"),
			uniqueConstraints = {
					@UniqueConstraint(name = "uk_pj_ramos", columnNames = {"ramosAtividade_id"})})
	private List<RamoAtividade> ramosAtividade;
	
	@Column(columnDefinition = "numeric(10, 2)", nullable = false)
	private Double faturamento;
	
	@OneToMany(targetEntity = Vendedor.class)
	@CollectionTable(name = "tab_pj_vendedor", foreignKey = @ForeignKey(name="fk_vend_cnpj"),
	joinColumns = @JoinColumn(name = "cnpj_pessoa_juridica"), uniqueConstraints = {
			 @UniqueConstraint(name = "uk_pj_vendedor", columnNames = {"vendedores_cpf"})})
	private List<Vendedor> vendedores;

	public PessoaJuridica() {
		super();
	}

	public PessoaJuridica(String cnpj, String nome, List<RamoAtividade> ramosAtividade, Double faturamento,
			List<Vendedor> vendedores) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
		this.ramosAtividade = ramosAtividade;
		this.faturamento = faturamento;
		this.vendedores = vendedores;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamosAtividade() {
		return ramosAtividade;
	}

	public void setRamosAtividade(List<RamoAtividade> ramosAtividade) {
		this.ramosAtividade = ramosAtividade;
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + ((faturamento == null) ? 0 : faturamento.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((ramosAtividade == null) ? 0 : ramosAtividade.hashCode());
		result = prime * result + ((vendedores == null) ? 0 : vendedores.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
	PessoaJuridica other = (PessoaJuridica) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (faturamento == null) {
			if (other.faturamento != null)
				return false;
		} else if (!faturamento.equals(other.faturamento))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (ramosAtividade == null) {
			if (other.ramosAtividade != null)
				return false;
		} else if (!ramosAtividade.equals(other.ramosAtividade))
			return false;
		if (vendedores == null) {
			if (other.vendedores != null)
				return false;
		} else if (!vendedores.equals(other.vendedores))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "pessoaJuridica [cnpj=" + cnpj + ", nome=" + nome + ", ramosAtividade=" + ramosAtividade
				+ ", faturamento=" + faturamento + ", vendedores=" + vendedores + "]";
	}

}
