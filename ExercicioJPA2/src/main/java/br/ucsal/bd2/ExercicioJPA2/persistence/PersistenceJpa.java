package br.ucsal.bd2.ExercicioJPA2.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceJpa {

	public static void main(String[] args) {

		// https://www.objectdb.com/java/jpa

		EntityManagerFactory emf = null;

		try {

			emf = Persistence.createEntityManagerFactory("exercicio-jpa02-pu");
			EntityManager em = emf.createEntityManager();

			em.getTransaction().begin();

			em.getTransaction().commit();

		} finally {

			if (emf != null) {
				emf.close();
			}

		}

	}

}
